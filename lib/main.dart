import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:session0new/presentation/pages/map.dart';
import 'package:session0new/presentation/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


Future<void> main() async {
  await Supabase.initialize(
    url: 'https://uboklrrvwysdoztwfvuj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVib2tscnJ2d3lzZG96dHdmdnVqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcyOTI1MjEsImV4cCI6MjAyMjg2ODUyMX0.snO7gjARZRw677ToKsGYc49lgiHIU7HCaCw6M7rIXwY',

  );
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {

  MyApp({super.key});






  @override
  State<MyApp> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> _connectivitySubscription;
  void onChangeTheme() {
    setState(() {});
  }
  @override
  void initState() {
    super.initState();
    _connectivitySubscription = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){
        showError(context, 'Network crashed');
      }
    });
  }
  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MapPage(),
    );
  }
}

