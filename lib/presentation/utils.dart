import 'package:flutter/material.dart';

void hideLoading(BuildContext context){
  Navigator.of(context).pop();
}

Future<void> showError(BuildContext context, String error) async {
  await showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ошибка"),
    content: Text(error),
    actions: [
      TextButton(onPressed: (){Navigator.pop(context);}, child: const Text("OK"))
    ],
  ));
}

void showLoading(BuildContext context)async{
  showDialog(context: context,
      barrierDismissible: false,
      builder: (_)=> const PopScope(
    canPop: false,
      child: Dialog(
    backgroundColor: Colors.transparent,
    surfaceTintColor: Colors.transparent,

    child: Center(child: CircularProgressIndicator(),),
  )));
}