import 'package:flutter/material.dart';
import 'package:session0new/data/repository/shared.dart';
import 'package:session0new/domain/map_use_case.dart';
import 'package:session0new/presentation/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:yandex_mapkit/yandex_mapkit.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  String initials = '';
  SharedPreferences? sharedPreferences;
  var email = TextEditingController();
  var password = TextEditingController();

  @override
  void initState(){
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      sharedPreferences = value;
      initials = await getCheck(sharedPreferences!);
      if (initials != ''){
        await logining(initials);
      }
    });

  }





  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}