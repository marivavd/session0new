import 'package:flutter/material.dart';
import 'package:session0new/domain/map_use_case.dart';
import 'package:session0new/presentation/utils.dart';

import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {

  MapUseCase useCase = MapUseCase();
  String address = '';

  double? latitude, longitude;

  @override
  void initState() {
    super.initState();
    useCase.getCurrentLocation(
        onResponse: (position) async {
          await useCase.geocodePoint(
              Point(
                  latitude: position.latitude,
                  longitude: position.longitude
              ),
              onResponse: (String address1) {
                address = address1;
                setState(() {
                  print(address);
                });

              },
              onError: (String error) {
                showError(context, error);
              });
        },
        onError: (error){
            showError(context, error);
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return (address != '')?Scaffold(
      body: Center(
        child: Text(
            address
        ),
      ),

    ):Center(child: CircularProgressIndicator(),);
  }
}